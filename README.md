Pingstat
========

A wrapper around `ping` that shows a periodic summary.


License
-------

GPLv3+, see COPYING


Sources
-------

https://gitlab.com/mattia.basaglia/Pingstat


Author
------

Mattia Basaglia <mattia.basaglia@gmail.com>
