#!/usr/bin/env python3
#
# Copyright (C) 2016-2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import re
import sys
import time
import argparse
import datetime
import threading
import subprocess
from collections import defaultdict

def dotter():
    while True:
        sys.stdout.write('.')
        sys.stdout.flush()
        time.sleep(1)

def clear_dotter():
    sys.stdout.write('\r')

def pc_color(x):
	color = "31"
	if x == "":
		color = "22;31"
	elif int(x) == 0:
		color = "32"
	elif int(x) < 33:
		color = "33"
	return "\x1b[1;%sm" % color

def why_u_so_whiny_python(regex, subject):
    return defaultdict(str, regex.search(subject.decode('utf-8')).groupdict())


parser = argparse.ArgumentParser(
    description="Shows ping status at regular intervals"
)
parser.add_argument(
    "host",
    default="192.168.1.1",
    help="host to ping",
    nargs='?',
)
parser.add_argument(
    "--interval",
    type=int,
    default=10,
    help="Number of seconds between stats",
)
parser.add_argument(
    "--format",
    default="{received}/{sent} {loss}% pl {time}ms {minmax}",
    help="Message to display",
)
parser.add_argument(
    "--no-dot",
    default=True,
    help="Don't show progress dots",
    action="store_false",
    dest="dot",
)

options = parser.parse_args()
re_packets = re.compile(
    r"(?P<sent>[0-9]+).*(?P<received>[0-9]+)[^0-9]+(?:, \+[0-9]+ errors, )?(?P<loss>[0-9]+)%.[^0-9]+(?P<time>[0-9]+)ms"
)
re_time = re.compile(
    r"[^0-9.]+(?P<minmax>(?P<min>[0-9.]+)/(?P<avg>[0-9.]+)/(?P<max>[0-9.]+))/(?P<mdev>[0-9.]+) ms"
)

if options.dot:
    threading.Thread(target=dotter, daemon=True).start()

ping_cmd = ["ping", "-q", "-w", str(options.interval), options.host]

try:
    while True:
        before = time.time()
        out, err = subprocess.Popen(
            ping_cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        ).communicate()

        out_lines = out.splitlines()

        if len(out_lines) < 3:
            message = "Not connected"
            color = "\x1b[31m"
        else:
            try:
                data = why_u_so_whiny_python(re_packets, out_lines[3])
                if len(out_lines) > 3 and out_lines[4].startswith(b"rtt") and out_lines[4]:
                    data.update(why_u_so_whiny_python(re_time, out_lines[4]))
                message = options.format.format_map(data)
                color = pc_color(data["loss"])
            except Exception as e:
                message = "You done f'd up now: %s\n%s" % (e, out)
                color = "\x1b[35m"

        clear_dotter()
        print(
            color + datetime.datetime.now().isoformat() + " " + message + "\x1b[m"
        )

        # Ensure it never runs more often than inteval
        time_to_interval = options.interval - (time.time() - before)
        if time_to_interval > 0:
            time.sleep(time_to_interval)
except KeyboardInterrupt:
    pass
